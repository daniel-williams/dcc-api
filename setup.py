#!/usr/bin/env python
# -*- coding: utf-8 -*-


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = [
    "appdirs==1.4.0",
    "beautifulsoup4==4.5.3",
    "html2text==2016.9.19",
    "kerberos==1.2.5",
    "packaging==16.8",
    "pyparsing==2.1.10",
    "pytz==2016.10",
    "six==1.10.0"
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='dcc-api',
    version='0.0.1',
    description="An API for the LIGO Scientific Collaboration Document Control Centre (DCC).",
    long_description=readme + '\n\n' + history,
    author="Sean Leavey",
    author_email='sean.leavey@ligo.org',
    url='https://git.ligo.org/sean-leavey/dcc-api',
    packages=[
        'dcc',
    ],
    package_dir={'dcc':
                 'dcc'},
    include_package_data=True,
    install_requires=requirements,
    license="GPLv3",
    zip_safe=False,
    keywords='minke',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    #test_suite='tests',
    #tests_require=test_requirements
)
